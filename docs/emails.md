# Emails

## Defining emails

At the beginning lets define email group:
```
rails generate mailer ArticleMailer
```

* Explain what is generated
* Show defaults in `ApplicationMailer`
* My prefference: haml for html, erb for text

At the beginning lets define test for our mailer
(`spec/mailer/article_mailer_spec.rb`):
```ruby
require 'rails_helper'

RSpec.describe ArticleMailer do
  let(:article) { build(:article) }
  let(:mail) { ArticleMailer.article_published_email(article) }

  it 'renders the headers' do
    expect(mail.subject).to eq 'Article published'
    expect(mail.to).to eq [article.user.email]
    expect(mail.from).to eq ['from@example.com']
  end

  it 'renders the body' do
    expect(mail.body.encoded).to match('Huray!')
    expect(mail.body.encoded).to match(article.title)
  end
end
```

And lets implement this email
```ruby
class ArticleMailer < ApplicationMailer

  def article_published_email(article)
    author = article.user
    @title = article.title

    mail(to: author.email, subject: 'Article published')
  end
end
```

And mail templates (for both html and text).

`app/views/article_mailer/article_published_email.html.haml`:
```haml
%p Huray!!!
%p your article "#{@title}" has just been published!
```

`app/views/article_mailer/article_published_email.text.erb`:
```erb
Huray!!!
your article <%= @title %> has just been published!
```

It would be nice to see how our email looks like. This is quite simple, just
create email preview (`spec/mailers/previews/article_mailer_preview.rb`):
```ruby
class ArticleMailerPreview < ActionMailer::Preview
  def article_published_email
    article = Article.new(title: 'This is article title',
                            text: 'Article text',
                            user: User.new(email: 'mailer@preview.pl'))

    ArticleMailer.article_published_email(article)
  end
end
```

Since we are not using default testing framework but rspec, we need to tell
rails where previews are located. Add following configuration into
`config/application.rb`:
```ruby
config.action_mailer.preview_path = "#{Rails.root}/spec/mailers/previews"
```

* Add some header and footer into email template

## Sending emails

Requirement: email should be sent to article user after article is published.

Lets start from defining test scenario for this requirement
(`spec/requests/articles_spec.rb`):
```ruby
it 'sends email after article is published' do
  article = create(:article, status: :draft, user: user)

  expect do
    put article_path(article), params: { article: { status: 'published' } }
  end.to change { ActionMailer::Base.deliveries.count }.by(1)
end

it 'email is not changed while status not changed' do
  article = create(:article, status: :draft, user: user)

  expect do
    put article_path(article), params: { article: { title: 'New title'} }
  end.to_not change { ActionMailer::Base.deliveries.count }
end
```

and add implementation (`app/controllers/articles_controller.rb`):
```ruby
def update
  respond_to do |format|
    if @article.update(article_params)
      format.html { redirect_to @article, notice: 'Article was successfully updated.' }
      format.json { render :show, status: :ok, location: @article }
      ArticleMailer.article_published_email(@article).deliver_now if @article.publishing?
    else
      format.html { render :edit }
      format.json { render json: @article.errors, status: :unprocessable_entity }
    end
  end
end
```

and run tests:
```
bundle exec rspec
```

```
Randomized with seed 16405
.........F.............

Failures:

  1) ArticlesController when logged in email is not changed while status not
changed
     Failure/Error:
       expect do
         put article_path(article), params: { article: { title: 'New title'} }
       end.to_not change { ActionMailer::Base.deliveries.count }

       expected `ActionMailer::Base.deliveries.count` not to have changed, but
did change from 0 to 1
     # ./spec/requests/articles_spec.rb:62:in `block (3 levels) in <main>'

Finished in 0.78567 seconds (files took 0.77432 seconds to load)
23 examples, 1 failure

Failed examples:

rspec ./spec/requests/articles_spec.rb:59 # ArticlesController when logged in
email is not changed while status not changed

Randomized with seed 16405

```

We want to send "article published" email only when article status changed from
`:draft` into `:published` so lets create spec for that
(`spec/model/article_spec.rb`):
```ruby
context '#publishing?' do
  it 'is true when changing status from draft to published' do
    article = create(:article, status: :draft)

    article.update(status: :published)

    expect(article.publishing?).to be_truthy
  end

  it 'is false when article is draft' do
    article = create(:article, status: :draft)

    article.update(status: :draft)

    expect(article.publishing?).to be_falsy
  end

  it 'is false when article is already published' do
    article = create(:article, status: :published)

    article.update(status: :published)

    expect(article.publishing?).to be_falsy
  end
end
```

and add method implementation (`app/models/article.rb`):
```ruby
class Article < ApplicationRecord
  before_save :check_if_publishing

  def publishing?
    @publishing
  end

  def check_if_publishing
    @publishing = status_was == 'draft' && status == 'published'
  end
end
```

* Explain ActiveRecord `*_was`

now we can update line when email is sent
(`app/controllers/articles_controller.rb`):
```
ArticleMailer.article_published_email(@article).deliver_now if @article.publishing?
```

and run tests again:
```
bunde exec rspec
```
Randomized with seed 17310
.......................

Finished in 0.76423 seconds (files took 0.76243 seconds to load)
23 examples, 0 failures

Randomized with seed 17310
```
