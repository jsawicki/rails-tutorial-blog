# API

New requirement: REST API for managing articles secured by JWT token.

Add `jwt` gem into `Gemfile`:
```ruby
ruby 'jwt'
```

Firstly we will implement possibility to generate token and find user using JWT token.

`spec/models/user_spec.rb`:
```ruby
RSpec.describe User do

  it 'has token generation and finder' do
    user = create(:user)

    token = user.token
    found_user = User.from_token(token)

    expect(found_user).to eq user
  end
end
```

`app/models/user.rb`:
```ruby
class User < ApplicationRecord
  ...

  def self.from_token(token)
    User.find_by(email: decode_token(token)[0]['email'])
  end

  def token
    JWT.encode(token_payload,
               Rails.application.credentials.secret_key_base,
               'HS256')
  end

  private

  def token_payload
    {
      email: email,
      sub: id,
      iss: 'demo',
      exp: Time.now.to_i + 21600
    }
  end

  def self.decode_token(token)
    JWT.decode(token, Rails.application.credentials.secret_key_base, true,
               algorithm: 'HS256')
  end
end
```

Next we need API endpoint that will allow to generate user JWT token using
username/password.

`spec/requests/api/sessions_spec.rb`:
```ruby
require 'rails_helper'

RSpec.describe Api::SessionsController do
  it 'generates token after successful login' do
    user = create(:user, password: 'password')

    post api_sessions_path,
      params: { user: { email: user.email, password: 'password' } }

    expect(response.status).to eq 201
    data = JSON.parse(response.body)
    expect(data['user']['email']).to eq user.email
    expect(User.from_token(data['user']['token'])).to eq user
  end

  it 'returns 401 when email/password combination is wrong' do
    post api_sessions_path,
      params: { user: { email: 'a@b.pl', password: 'wrong' } }

    expect(response.status).to eq 401
  end
end
```

Create API controller base class:
```ruby
module Api
  class ApplicationController < ActionController::API
    include Pundit

    before_action :authenticate_user!
    before_action :destroy_session
    rescue_from Pundit::NotAuthorizedError, with: :forbidden

    def destroy_session
      request.session_options[:skip] = true
    end

    protected

    def api_error(status: 500, errors: [])
      head(status) && return if errors.empty?
      render json: jsonapi_format(errors).to_json, status: status
    end

    def jsonapi_format(errors)
      return errors if errors.is_a?(String)
      errors_hash = {}
      errors.messages.each do |attribute, error|
        array_hash = []
        error.each { |e| array_hash << { attribute: attribute, message: e } }
        errors_hash.merge!(attribute => array_hash)
      end

      errors_hash
    end

    private

    def authenticate_user!
      head :unauthorized, 'WWW-Authenticate' => error_401_message unless current_user
    end

    def error_401_message
      msg = 'Bearer realm="example"'
      msg += ', error="invalid_token"' if request.env['HTTP_AUTHORIZATION']

      msg
    end

    def forbidden
      api_error(status: :forbidden)
    end
  end
end
```

And possiblity to get token using API
(`app/controllers/api/sessions_controller.rb`):
```ruby
module Api
  class SessionsController < Api::ApplicationController
    skip_before_action :authenticate_user!

    def create
      user = User.find_by(email: create_params[:email])
      return api_error(status: 401) unless user&.valid_password?(create_params[:password])

      @current_user = user
      render json: user_details, status: 201
    end

    private

    def user_details
      {
        user: {
          email: @current_user.email,
          token: @current_user.token
        }
      }
    end

    def create_params
      params.require(:user).permit(:email, :password)
    end
  end
end
```

Now we can configure this endpoint in `config/routes.rb`:
```ruby
Rails.application.routes.draw do
  ...

  namespace :api do
    resources :sessions, only: :create
  end
end
```

After these changes our tests should pass:
```
bundle exec rspec spec/requests/api/sessions_spec.rb
```

```
Randomized with seed 14109
..

Finished in 0.11589 seconds (files took 0.84918 seconds to load)
2 examples, 0 failures
```

Now we can fetch JWT token using e.g. `curl` command:
```
curl -v -X POST --header "Content-Type:application/json" \
--data '{ "user": { "email": "foo@bar", "password": "secret" } }' \
http://localhost:3000/api/sessions
```

Now we are ready to create first real REST API.
`spec/requests/api/articles_spec.rb`:
```ruby
require 'rails_helper'

RSpec.describe Api::ArticlesController do
  context 'as non logged in user' do
    it 'there is no possibility to get articles list' do
      get api_articles_path

      expect(response.status).to eq 401
    end

    # other tests for articles api can be created here
  end

  context 'as a logged in user' do
    let(:user) { create(:user) }
    let(:token) { user.token }

    it 'I can list my and published articles' do
      create(:article, user: user)
      create(:article, title: 'published', status: :published)
      create(:article, title: 'not-visible', status: :draft)

      get api_articles_path,
        headers: { 'Authorization': "Bearer #{token}",
                   'Accept': 'application/json' }

      expect(response.status).to eq 200
      data = JSON.parse(response.body)
      expect(data['articles'].size).to eq 2
    end

    # Other tests for post/put/delete
  end
end
```
At the beginning lets define routing
(`config/routes.rb`):
```ruby
Rails.application.routes.draw do
  ...

  namespace :api do
    resources :sessions, only: :create
    resources :articles
  end
end
```

Now we can create empty API articles controller
(`app/controllers/api/articles_controller.rb`):
```ruby
module Api
  class ArticlesController < Api::ApplicationController
    def index
    end
  end
end
```

Now we can start our tests:
```
bundle exec rspec spec/requests/api/articles_spec.rb
```
```
Randomized with seed 16557
.F

Failures:

  1) Api::ArticlesController as a logged in user I can list my and published
articles
     Failure/Error: expect(response.status).to eq 200

       expected: 200
            got: 401

       (compared using ==)
     # ./spec/requests/api/articles_spec.rb:26:in `block (3 levels) in <top
(required)>'

Finished in 0.20755 seconds (files took 0.81746 seconds to load)
2 examples, 1 failure

Failed examples:

rspec ./spec/requests/api/articles_spec.rb:18 # Api::ArticlesController as a
logged in user I can list my and published articles
```

It seems like there is no JWT authentication method. Lets define new JWT
authentication devise strategy (`lib/devise/strategies/jwt_authenticatable.rb`):
```ruby
require 'devise/strategies/authenticatable'

module Devise
  module Strategies
    class JwtAuthenticatable < Authenticatable
      def valid?
        super || token && request.fullpath.starts_with?('/api')
      end

      def authenticate!
        resource = User.from_token(token)
        resource ? success!(resource) : fail(:invalid_credentials)
      rescue StandardError
        fail(:invalid_credentials)
      end

      private

      def token
        @token ||= bearer_token
      end

      def bearer_token
        pattern = /^Bearer /
        header  = request.env['HTTP_AUTHORIZATION']
        header.gsub(pattern, '') if header&.match(pattern)
      end
    end
  end
end

Warden::Strategies.add(:jwt, Devise::Strategies::JwtAuthenticatable)
```

And notify devise that we want to use this strategy
(`config/initializers/devise.rb`):
```ruby
require 'devise/strategies/jwt_authenticatable'

Devise.setup do |config|
  ...

  config.warden do |manager|
    manager.intercept_401 = false
    manager.default_strategies(scope: :user).unshift :jwt
  end
end
```

Now after restarting our test we can see other error:
```
bundle exec rspec spec/requests/api/articles_spec.rb
```
```
Randomized with seed 24257
.F

Failures:

  1) Api::ArticlesController as a logged in user I can list my and published
articles
     Failure/Error:
       get api_articles_path,
         headers: { 'Authorization' => "Bearer #{token}" }

     ActionController::UnknownFormat:
       Api::ArticlesController#index is missing a template for this request
format and variant.

       request.formats: ["text/html"]
       request.variant: []

       NOTE! For XHR/Ajax or API requests, this action would normally respond
with 204 No Content: an empty white screen. Since you're loading it in a web
browser, we assume that you expected to actually render a template, not nothing,
so we're showing an error to be extra-clear. If you expect 204 No Content, carry
on. That's what you'll get from an XHR or API request. Give it a shot.
     # ./spec/requests/api/articles_spec.rb:23:in `block (3 levels) in <top
(required)>'

Finished in 0.2359 seconds (files took 0.83122 seconds to load)
2 examples, 1 failure

Failed examples:

rspec ./spec/requests/api/articles_spec.rb:18 # Api::ArticlesController as a
logged in user I can list my and published articles
```

So now we can implement our `index` method and appropriate view template:

`app/controllers/api/articles_controller.rb`:
```ruby
module Api
  class ArticlesController < Api::ApplicationController
    def index
      authorize Article
      @articles = policy_scope(Article)
    end
  end
end
```

`app/views/api/articles/index.json.jbuilder`:
```ruby
json.articles do
  json.array! @articles, partial: 'articles/article', as: :article
end
```

As you can see here we are reusing partial defined for `/articles` resources.

## Pure API rails app

```
rails new my_api -api
```

## Other alternatives for JSON rendering

  * [Active model serializers](https://github.com/rails-api/active_model_serializers)
  * [jsonapi-rb](http://jsonapi-rb.org)
