# Basics

## Creating new rails application

Create new rails application with PostgreSQL database connector without standard
testing framework (`-T`).

```
rails new hello -T --database=postgresql
rails db:create
rails db:migrate
```

Start new created application

```
./bin/rails server
```

Describe what is generated. Don't forget to mentions about:

  * assets
  * controllers
  * views
  * layouts
  * models

## First webpage

```
rails generate controller Welcome index
```

* Show `app/controller/welcome_controller.rb` and
  `app/views/welcome/index.html.erb`
* Show `config/routes.rb`
* Add `welcome#index` as root route

## Dynamic content

Edit `app/views/welcome/index.html.erb` and change `<p>` tag into:

```erb
<p><%= Time.now %></p>
```

Explain why login in view is wrong and move it into controller:

`app/views/welcome/index.html.erb`:

```erb
<p><%= now %></p>
```

`app/controllers/welcome_controller.rb`

```ruby
def index
  @now = Time.now
end
```

Explain what is the difference between `variable_name` and `@variable_name`.

## Haml

Add into `Gemfile`:

```ruby
gem 'haml-rails'
```

and run:

```
bundle install
rails restart
```

Rename `app/views/welcome/index.html.erb` into
`app/views/welcome/index.html.haml` and chage payload into:

```haml
%h1 Welcome#index
%p= @now
```

## Bootstrap and font awesome

Add into `Gemfile`

```ruby
gem 'jquery-rails'
gem 'bootstrap', '~> 4.0.0.beta3'
gem 'font-awesome-sass', '~> 4.7'
```

and run:

```
bundle install
rails restart
```

move `app/assets/stylesheets/applicaiton.css` into
`app/assets/stylesheets/applicaiton.scss` and add:

```scss
@import "bootstrap";
@import "font-awesome-sprockets";
@import "font-awesome";
```

add into `app/assets/javascripts/application.js`:

```js
//= require jquery3
//= require popper
//= require bootstrap-sprockets
```

Create `app/views/layouts/_head.html.haml`

```haml
%head
  %title TutorialBlog
  = csrf_meta_tags

  = stylesheet_link_tag    'application', media: 'all', 'data-turbolinks-track': 'reload'
  = javascript_include_tag 'application', 'data-turbolinks-track': 'reload'
```

Move `app/views/layouts/application.html.erb` into
`app/views/layouts/application.html.haml` and change payload into:

```haml
!!! 5
%html
  = render 'layouts/head'
  %body
    %main.container{role: 'main'}
      = yield
```

Change `app/views/welcome/index.html.haml` payload into:

```haml
%h1 Bootstrap & font awesome
.alert.alert-primary
  = icon 'calendar'
  = @now
```

## CRUD

Run

```
rails g scaffold Article title:string text:text
rails db:migrate
```

* Play with CRUD a bit
* Show what is generated
* Explain migrations
* Show routes and output of `rails routes`
* Explain paths/urls generation (e.g. `root_path`, `articles_path`,
  `edit_article_path(@article)`
* Show article json

Add article `title` and `text` validation (in `app/models/article.rb`):

```ruby
class Article < ApplicationRecord
  validates :title, presence: true
  validates :text, presence: true

  after_save :log_creation

  private

  def log_creation
    logger.info("New article: #{title} created")
  end
end
```

* Explain validation
* Explain callbacks and why callbacks should be used with care

## Simple form

Add into `Gemfile`

```ruby
gem 'simple_form'
```

and run:

```
bundle install
rails generate simple_form:install --bootstrap
rails restart
```

change `app/views/articles/_form.html.haml` payload into:

```haml
= simple_form_for @article do |f|
  = f.input :title
  = f.input :text
  = f.button :submit
```

To change title, error messages add into `config/locales/simple_form.en.yml`

```
labels:
  article:
    title: Article title
    text: Article text
```

## I18n

Change `h1` tag in `app/views/welcome/index.html.haml` into:

```haml
%h1= t 'welcome.index.title'
```

and add into `config/locales/en.yml`:

```yaml
en:
  welcome:
    index:
      title: Bootstrap & font awesome
```

Change `h1` tag in `app/views/welcome/index.html.haml` into:

```haml
%h1= t '.title'
```

* Explain translation
* Explain local translation (with controller, action scope)

# Console

Run:

```
rails console
```

and find e.g. all articles, create new article:

```
Article.all
Article.create(title: 'from console', text: 'article text')
```
