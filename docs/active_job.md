# Active jobs

Since ruby has GIT (global interpreter lock) some of time consuming tasksr
(e.g. email sending) should be moved into separate thread.

## Sidekiq

Add new dependency to your `Gemfile`
```ruby
gem 'sidekiq'
```

Now we need totell rails that we want to use it as our delayed job executor.
Create `config/initializers/active_job.rb`:
```ruby
ActiveJob::Base.queue_adapter = Rails.env.test? ? :inline : :sidekiq
```
Since sidekiq is running as a separate process we need to start it once we are
starting our application. To simplify this process we can use `formen`.
```
gem install foreman
```

Now we can create foreman configuration (`Procfile`):
```
web: PORT=3000 rails server
jobs: bundle exec sidekiq -q default -q mailers
```

By default delayed emails are sent to `emailers` queue. Additionally we are
configuring sidekiq to listen also on `default` queue.

Now we can modify our code to use active jobs. Change
`app/controllers/atricles_controller.rb`:
```ruby
ArticleMailer.article_published_email(@article).deliver_later if @article.publishing?
```

And start application:
```
foreman start
```

## Defining new active job

```
rails generate job article_indexer
```

create simple implementation (`app/jobs/article_indexer_job.rb`):
```ruby
class ArticleIndexerJob < ApplicationJob
  queue_as :default

  def perform(article)
    sleep(10)
    Rails.logger.info("Indexer mock invoked after 10 seconds for #{article.title}")
  end
end
```

Now we can invoke this job when article is published (
`app/controllers/article_controller.rb`):
```
if @article.publishing?
  ArticleMailer.article_published_email(@article).deliver_later
  ArticleIndexerJob.perform_later(@article)
end
```
