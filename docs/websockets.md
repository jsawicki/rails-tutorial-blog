# Websockets

We have new requirement: After new article is published and indexed all logged
in users should be notified.

First we will establish websocket connection only for logged in users
(`app/channels/application_cable/connection.rb`):
```ruby
module ApplicationCable
  class Connection < ActionCable::Connection::Base
    identified_by :current_user

    def connect
      self.current_user = find_verified_user
      logger.add_tags 'ActionCable', current_user.email
    end

    private

    def find_verified_user
      verified_user = User.find_by(id: cookies.signed['user.id'])

      verified_user || reject_unauthorized_connection
    end
  end
end
```

Now we need to add signed `user.id` field into cookies. Create
`config/initializers/warden_hooks.rb`:
```ruby
Warden::Manager.after_set_user do |user, auth, opts|
  scope = opts[:scope]
  auth.cookies.signed["#{scope}.id"] = user.id
end

Warden::Manager.before_logout do |_user, auth, opts|
  scope = opts[:scope]
  auth.cookies.signed["#{scope}.id"] = nil
end
```

* Explain what is "Warden"

And since in development mode we are using http we need to relax security policy
for localhost (`config/initializers/content_security_policy.rb`):
```ruby
p.connect_src :self, :https, "http://localhost:3000", "ws://localhost:3000"
```

Change action cable backend configuration (`config/cable.yml`):
```yaml
redis: &redis
  adapter: redis
  url: <%= ENV['REDIS_URL'] || 'redis://localhost:6379/4' %>

production:
  <<: *redis
  channel_prefix: ryszard_production

development:
  <<: *redis
  channel_prefix: ryszard_development

test:
  adapter: async
```

Since we changed application configuration we need to restart it:
```
rails restart
```

Now we can create new notification channel (`app/channels/notification_channel.rb`):
```ruby
class NotificationChannel < ApplicationCable::Channel
  def subscribed
    stream_for 'notification'
  end
end
```

and simple JS which handle this connection
(`app/assets/javascripts/channels/notification.coffee`):
```coffee
$(document).on "turbolinks:load", ->
  App.notification = App.cable.subscriptions.create {
      channel: "NotificationChannel"
    },
    connected: ->
      console.log("ws connected")

    received: (data) ->
      console.log("data received %o", data)
      @notify(data.msg, data.status)

    notify: (msg, status) ->
      document.getElementById("alerts").
        innerHTML = """
          <div class="alert alert-#{status} alert-dismissible fade show" role="alert">
            #{msg}
            <button class="close" type="button" data-dismiss="alert" />
            <span aria-hidden="true">&times</span>
          </div>
        """
```

Last thing we need to change is to add `alerts` tag in the flash section
(`app/views/layouts/_flash.html.haml`):
```haml
#alerts
  - if alert
    .alert.alert-danger.alert-dismissible.fade.show{role: 'alert'}
      = alert
      %button.close{type: 'button', data: { dismiss: 'alert' }}
        %span{"aria-hidden" => "true"} &times;

  - elsif notice
    .alert.alert-primary.alert-dismissible.fade.show{role: 'alert'}
      = notice
      %button.close{type: 'button', data: { dismiss: 'alert' }}
        %span{"aria-hidden" => "true"} &times;
```

Now we can play with action cable using console:
```
rails console
```

```ruby
NotificationChannel.broadcast_to('notification',
                                 msg: 'something inportant',
                                 status: 'danger')
```

We can use it everywhere in the application. E.g. we can add it into delayed
indexer job (`app/jobs/article_indexer_job.rb`):
```ruby
class ArticleIndexerJob < ApplicationJob
  queue_as :default

  def perform(article)
    sleep(10)
    Rails.logger.info("Indexer mock invoked after 10 seconds for #{article.title}")

    if article.published?
      NotificationChannel.
        broadcast_to('notification',
                     msg: "New article published: #{article.title}",
                     status: 'success')
    end
  end
end
```
