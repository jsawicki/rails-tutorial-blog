# Testing

## Installation

During this tutorial we will be using rspec as our testing framework. To install
it add following dependency into `Gemfile` test group:

```ruby
gem 'rspec-rails', '~> 3.7'
gem 'factory_bot_rails'
gem 'faker'
gem 'shoulda-matchers'
gem 'capybara'
```

run:

```
bundle install
rails generate rspec:install
```

`shoulda-matchers` configuration in `spec/support/shoulda-matchers.rb`:
```ruby
Shoulda::Matchers.configure do |config|
  config.integrate do |with|
    with.test_framework :rspec
    with.library :rails
  end
end
```

`factory_bot` configuration in `spec/support/factory_bot.rb`:
```ruby
RSpec.configure do |config|
  config.include FactoryBot::Syntax::Methods
end
```

Uncomment following line in `spec/rails_helper.rb` to load additional
configuration from `spec/support` directory:

```diff
-# Dir[Rails.root.join('spec/support/**/*.rb')].each { |f| require f }
+Dir[Rails.root.join('spec/support/**/*.rb')].each { |f| require f }
```

remove use fixtures since we will be using factory bot to create db entries:

```diff
-  # Remove this line if you're not using ActiveRecord or ActiveRecord fixtures
-  config.fixture_path = "#{::Rails.root}/spec/fixtures"
```

and add capybara import:

```diff
+require 'capybara/rails'
```


* Explain difference between `spec_helper.rb` and `rails_helper.rb`
* Explain how factory and faker are used to simplify test creation

## Integration tests - loging and creating new article

### Login features tests

Don't connect real FB or Google during tests (`spec/support/omniauth.rb`):
```ruby
OmniAuth.config.test_mode = true
```

Create module with login helper methods
(`spec/support/helper/authentication_helper.rb`):
```ruby
module AuthenticationHelper
  def google_sign_in_as(user)
    stub_oauth(
      :google_oauth2,
      email: user.email
    )
    visit user_google_oauth2_omniauth_authorize_path
  end

  def facebook_sign_in_as(user)
    stub_oauth(
      :facebook,
      email: user.email
    )
    visit user_facebook_omniauth_authorize_path
  end

  def sign_in_as(user)
    visit new_user_session_path

    fill_in 'Email', with: user.email
    fill_in 'Password', with: user.password
    click_button 'Log in'
  end

  def stub_oauth(provider, options = {})
    OmniAuth.config.add_mock(
      provider,
      info: {
        email: options[:email],
      },
      provider: provider,
      uid: '123'
    )
  end
end
```

To simplify user creation we can use factory bot. It requires from us to create
user template (in `spec/factories/users.rb`):
```ruby
FactoryBot.define do
  factory :user do
    email { Faker::Internet.unique.email }
    password '12345678'
  end
end
```

And our login integration test (`spec/features/login_spec.rb`):
```ruby
require 'rails_helper'

RSpec.feature 'Login' do
  include AuthenticationHelper

  it 'using FB creates new user if not exist' do
    user = build(:user)

    expect do
      facebook_sign_in_as(user)
    end.to change { User.count }.by(1)

    expect(User.last.email).to eq user.email
  end

  it 'using Google creates new user if not exist' do
    user = build(:user)

    expect do
      google_sign_in_as(user)
    end.to change { User.count }.by(1)

    expect(User.last.email).to eq user.email
  end

  context 'normal email/password login' do
    it 'does not create new user' do
      user = build(:user)

      sign_in_as(user)

      expect(page).to have_text 'Invalid Email or password.'
    end

    it 'works for already registered user' do
      user = create(:user)

      sign_in_as(user)

      expect(page).to have_text 'Signed in successfully.'
   end
  end
end
```

* Explain what `create`, `build`, `expect` does.
* Explain capybara.

## Article tests

Lets create some more integration tests for creating and editing article. Here
we will be using acticles model a lot. To simplify this process lets create
template for factory bot (in `spec/factories/articles.rb`):
```ruby
FactoryBot.define do
  factory :article do
    title { Faker::Lorem.sentence }
    text { Faker::Lorem.paragraph }
    user # Factory bot please create related object if needed
  end
end
```

Create and edit article (`spec/features/article_lifecycle_spec.rb`):
```ruby
require 'rails_helper'

RSpec.feature 'Article' do
  include AuthenticationHelper

  it 'can be created by any logged in user' do
    author = create(:user)

    sign_in_as(author)
    visit new_article_path
    fill_in 'Article title', with: 'My title'
    fill_in 'Article text', with: 'My body'
    click_button 'Create Article'

    expect(page).to have_text 'My title'
    expect(page).to have_text 'My body'
    expect(page).to have_text author.email
    expect(page).to have_text 'draft'
  end

  it 'draft can be edited by article author' do
    author = create(:user)
    article = create(:article, user: author)

    sign_in_as(author)
    visit edit_article_path(article)
    fill_in 'Article title', with: 'My updated title'
    fill_in 'Article text', with: 'My updated body'
    click_button 'Update Article'

    expect(page).to have_text 'My updated title'
    expect(page).to have_text 'My updated body'
  end
end
```

* Show how `let` and `before` can be used here to simplify the code.

We can add more requests tests to test, e.g. validation
(`spec/requests/articles_spec.rb`):

```ruby
require 'rails_helper'

RSpec.describe ArticlesController do
  context 'when user not logged in' do
    it 'cannot see articles' do
      get articles_path

      expect(response).to redirect_to new_user_session_path
    end

    # similar to cannot create, edit, delete, see single article
  end

  context 'when logged in' do
    let(:user) { create(:user) }
    before { login_as(user) }

    it 'can see all created articles' do
      create(:article, user: user, title: 'Dogs vs Cats')
      create(:article, title: 'Other user article not published yet')

      get articles_path

      expect(response.status).to eq 200
      expect(response.body).to include 'Dogs vs Cats'
      expect(response.body).to_not include 'Other user article'
    end

    it 'can create new article' do
      post articles_path,
           params: { article: { title: 'title', text: 'text' } }

      article = Article.last

      expect(article.title).to eq 'title'
      expect(article.text).to eq 'text'
      expect(article.user).to eq user
      expect(article).to be_draft
    end

    it 'can edit owned article' do
      article = create(:article, user: user, title: 'Dogs vs Cats')

      put article_path(article),
          params: { article: { title: 'New title' } }
      article.reload

      expect(article.title).to eq 'New title'
    end

    it 'cannot edit not owned article' do
      article = create(:article, title: 'Other user article')

      put article_path(article),
          params: { article: { title: 'My precious' } }
      article.reload

      expect(response.body).to include 'redirected'
      expect(article.title).to eq 'Other user article'
    end
  end
end
```

## Sluggable article

It would be nice to have meaningfull article URL instead of using record id.
Let's try to do this but using TDD methodology - test first!

### Model test

At the beginning create test with behavior you wish to have, don't think about
implementation (`spec/models/article_spec.rb`):

```ruby
require 'rails_helper'

RSpec.describe Article do
  context 'slugable' do
    it 'create slug after record is stored in db' do
      article = create(:article, title: 'My new article')

      expect(article.slug).to eq 'my-new-article'
    end
  end
end
```
Now lets try to run our tests by funning:
```
bundle exec rspec spec/models/article_spec.rb
```

The result is as follow:
```
andomized with seed 63211
F

Failures:

  1) Article slugable create slug after record is stored in db
     Failure/Error: expect(article.slug).to eq 'my-new-article'

     NoMethodError:
       undefined method `slug' for #<Article:0x0000000003557e38>
     # ./spec/models/article_spec.rb:8:in `block (3 levels) in <top (required)>'

Finished in 0.09922 seconds (files took 0.6661 seconds to load)
1 example, 1 failure

Failed examples:

rspec ./spec/models/article_spec.rb:5 # Article slugable create slug after
record is stored in db
```

To solve this problem we need to create new migration which adds this field:
```
rails generate migration add_slug_to_article
```

```ruby
class AddSlugToArticle < ActiveRecord::Migration[5.2]
  def change
    add_column :articles, :slug, :string, null: false
  end
end
```

Right now our test is failing again:
```
andomized with seed 54135
F

Failures:

  1) Article slugable create slug after record is stored in db
     Failure/Error: article = create(:article, title: 'My new article')

     ActiveRecord::NotNullViolation:
       PG::NotNullViolation: ERROR:  null value in column "slug" violates not-null constraint
       DETAIL:  Failing row contains (1, My new article, Repellendus veritatis
consequatur error. Ullam vel non ut simili..., 2018-01-15 11:24:24.231612,
2018-01-15 11:24:24.231612, 1, 0, null).
       : INSERT INTO "articles" ("title", "text", "created_at", "updated_at",
"user_id") VALUES ($1, $2, $3, $4, $5) RETURNING "id"
     # ./spec/models/article_spec.rb:6:in `block (3 levels) in <top (required)>'
     # ------------------
     # --- Caused by: ---
     # PG::NotNullViolation:
     #   ERROR:  null value in column "slug" violates not-null constraint
     #   DETAIL:  Failing row contains (1, My new article, Repellendus veritatis
consequatur error. Ullam vel non ut simili..., 2018-01-15 11:24:24.231612,
2018-01-15 11:24:24.231612, 1, 0, null).
     #   ./spec/models/article_spec.rb:6:in `block (3 levels) in <top
(required)>'

Finished in 0.09975 seconds (files took 2.09 seconds to load)
1 example, 1 failure

Failed examples:

rspec ./spec/models/article_spec.rb:5 # Article slugable create slug after
record is stored in db

Randomized with seed 54135
```

Lets try to fix this by adding slug gneration before db record is created
(`app/models/article.rb`):
```ruby
class Article < ApplicationRecord
  ...
  before_create :slugify

  private

  def slugify
    self.slug = to_slug(title)
  end

  def to_slug(str)
    value = str.mb_chars.normalize(:kd).gsub(/[^\x00-\x7F]/n, '').to_s
    value.gsub!(/[']+/, '')
    value.gsub!(/\W+/, ' ')
    value.strip!
    value.gsub!(' ', '-')
    value.gsub!('_', '-')
    value.downcase
  end
  ...
end
```

Now it is much more better (run `bundle exec rspec`):

```
Randomized with seed 18869
.

Finished in 0.15958 seconds (files took 0.72881 seconds to load)
1 example, 0 failures
```

Slugable is quite generic thing which does not fit into article class. Lets try
to externalize it into "concern" (`app/models/concerns/slugable.rb`):
```ruby
module Slugable
  extend ActiveSupport::Concern

  def to_slug(str)
    value = str.mb_chars.normalize(:kd).gsub(/[^\x00-\x7F]/n, '').to_s
    value.gsub!(/[']+/, '')
    value.gsub!(/\W+/, ' ')
    value.strip!
    value.gsub!(' ', '-')
    value.gsub!('_', '-')
    value.downcase
  end
end
```

remove `to_slug` method from `Article` and include `Slugable` concern:
```ruby
class Article < ApplicationRecord
  include Slugable
  ...
end
```

To use new created `slug` field instad of `id` field add following method into
`Article` model:
```ruby
class Article < ApplicationRecord
  def to_param
    slug
  end
  ...
end
```

Arter this change we can create if our appliction is working propertly:

```
bundle exec rspec
```

```
Randomized with seed 8177
....FF.F.F..

Failures:

  1) Article can be created by any logged in user
     Failure/Error: @article = Article.find(params[:id])

     ActiveRecord::RecordNotFound:
       Couldn't find Article with 'id'=my-title
     # ./app/controllers/articles_controller.rb:70:in
`set_and_authorize_article'
     # ./spec/features/article_lifecycle_spec.rb:13:in `block (2 levels) in <top
(required)>'

  2) Article draft can be edited by article author
     Failure/Error: @article = Article.find(params[:id])

     ActiveRecord::RecordNotFound:
       Couldn't find Article with 'id'=quia-consequatur-non-reiciendis-porro-qui
     # ./app/controllers/articles_controller.rb:70:in
`set_and_authorize_article'
     # ./spec/features/article_lifecycle_spec.rb:26:in `block (2 levels) in <top
(required)>'

  3) ArticlesController when logged in cannot edit not owned article
     Failure/Error: @article = Article.find(params[:id])

     ActiveRecord::RecordNotFound:
       Couldn't find Article with 'id'=other-user-article
     # ./app/controllers/articles_controller.rb:70:in
`set_and_authorize_article'
     # ./spec/requests/articles_spec.rb:54:in `block (3 levels) in <main>'

  4) ArticlesController when logged in can edit owned article
     Failure/Error: @article = Article.find(params[:id])

     ActiveRecord::RecordNotFound:
       Couldn't find Article with 'id'=dogs-vs-cats
     # ./app/controllers/articles_controller.rb:70:in
`set_and_authorize_article'
     # ./spec/requests/articles_spec.rb:44:in `block (3 levels) in <main>'

Finished in 0.60498 seconds (files took 0.70278 seconds to load)
12 examples, 4 failures

Failed examples:

rspec ./spec/features/article_lifecycle_spec.rb:6 # Article can be created by
any logged in user
rspec ./spec/features/article_lifecycle_spec.rb:21 # Article draft can be edited
by article author
rspec ./spec/requests/articles_spec.rb:51 # ArticlesController when logged in
cannot edit not owned article
rspec ./spec/requests/articles_spec.rb:41 # ArticlesController when logged in
can edit owned article
```

Ups. Lets take a look at the source of our troubles
(`app/controllers/articles_controller.rb` `set_and_authorize_article` method).
Now we should search articles by `slug` not by `it`. Lets try to fix it:
```ruby
def set_and_authorize_article
  @article = Article.find_by(slug: params[:id])
  authorize @article
end
```

* Show also rails error page and explain what is visible there and how to use
  attached console.

and run our tests once again:
```
bundle exec rspec
```

```
Randomized with seed 34310
............

Finished in 0.6472 seconds (files took 0.73888 seconds to load)
12 examples, 0 failures
```

## Shoulda matchers

Set of one-liners which tests common Rails functionality, such as routing,
validation, relations, etc.

Lets add `Article` model validation and `User` relation tests:

`spec/models/article_spec.rb`:
```ruby
it { should validate_presence_of(:title) }
it { should validate_presence_of(:text) }
it { should belong_to(:user) }
```

`spec/models/user_spec.rb`:
```ruby
it { should have_many(:articles).dependent(:destroy) }
```
## Guard

Guard monitors files which was changed and run reflected tests

Install guard
```ruby
group :development do
  gem 'guard-rspec', require: false
end
```

and install this dependency:
```
bundle install
bundle exec guard init rspec
```

As a result `Guardfile` is generated. It define correlation between source
file(s) and test(s). Now guard can be started:
```
bundle exec guard
```

Every time some file is modified correlated tests will be executed (try to
comment/uncomment some validation from `Article` class).

* Explain what is inside `Guardfile`
## Setting up continous integration using gitlab-ci

Create database configuration specific for gitlab-ci
(`config/database.yml.gitlab-ci`):
```yaml
test:
  adapter: postgresql
  database: vapor_test
  username: vapor
  password: ""
  host: postgres
```

and GitLab CI configuratoin `.gitlab-ci.yml`:
```yaml
image: ruby:2.5

services:
  - postgres

variables:
  POSTGRES_DB: hello_test
  POSTGRES_USER: hello
  POSTGRES_PASSWORD: ""

cache:
  paths:
  - vendor/

stages:
  - test

test:
  stage: test
  script:
  - curl -sL https://deb.nodesource.com/setup_9.x | bash -
  - curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
  - echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
  - apt-get update
  - apt-get install -y nodejs yarn
  - ruby -v
  - gem install bundler --no-ri --no-rdoc
  - bundle install --path vendor --jobs $(nproc)  "${FLAGS[@]}"
  - cp config/database.yml.gitlab-ci config/database.yml
  - bundle exec rake db:create RAILS_ENV=test
  - bundle exec rake db:migrate RAILS_ENV=test
  - bundle exec rspec
```

Add new secret variable `RAILS_MASTER_KEY` into your Gitlab project settings
with `config/master.key` config or create new dedicated encrypted credentials
for test purpose (this is better approach).
