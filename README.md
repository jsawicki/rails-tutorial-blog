# Rails tutorial based on simple blog application

To start application `config/master.key` file need to be created. It is used to
decrypt application secrets (such as key for signing session cookie or
credentials allowing to login using FB, G+. If you need this file contact
@mkasztelnik.

## Ruby basics

* Show module, class, methods, inheritance, `attr_*`
* Show `do_that if something?`
* Show console

## Index
  * [Basics](docs/basics.md):
    - Create simple MVC application (with postgresql as a backend)
    - Create first static webpage (show how routing works)
    - Add dynamic content (e.g. Time.now)
    - Move dynamic content into controller
    - Present haml
    - Some styling with bootstrap and font awesome
    - Create first entity (e.g. Article) with CRUD
    - Present simple_form
    - Show console
  * [Authentication & Authorization](docs/aa.md)
    - Introduce Devise
    - Show reset/edit password capabilities
    - Show how to customize email messages
    - Omniauth and authentication using google, fb (google and fb need to be
      configured in advance)
    - Add relation between Post and User (db migration)
    - Introduce pundit (only post owner can edit/delete, but any logged in user
      can see)
  * [Tests](docs/tests.md)
    - Model tests (test Article slugable)
    - Show guard (optional)
    - Requests tests
    - Feature tests (mention about feature tests with JS and chrome headless)
  * [Emails](docs/emails.md)
    - Send email to Article author when new article is published
  * [API](docs/api.md)
    - Add api to existing rails application
    - Test api
    - New pure API rails app
  * [(Advanced) Background jobs](docs/active_jobs.md)
    - Configure email async send
    - Custom background processing (e.g. calculate Post hsh with some sleep)
  * [(Advanced) Websockets](docs/websockets.md)
    - Simple page update from console (e.g. show notification to all users)
    - Notification sent from delayed job
