class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,
         :omniauthable, omniauth_providers: [:facebook, :google_oauth2]

  has_many :articles, dependent: :destroy

  def self.from_omniauth(auth)
    where(email: auth.info.email).first_or_create do |user|
      user.password = Devise.friendly_token[0, 20]
    end
  end

  def self.from_token(token)
    User.find_by(email: decode_token(token)[0]['email'])
  end

  def token
    JWT.encode(token_payload,
               Rails.application.credentials.secret_key_base,
               'HS256')
  end

  private

  def token_payload
    {
      email: email,
      sub: id,
      iss: 'demo',
      exp: Time.now.to_i + 21600
    }
  end

  def self.decode_token(token)
    JWT.decode(token, Rails.application.credentials.secret_key_base, true,
               algorithm: 'HS256')
  end
end
