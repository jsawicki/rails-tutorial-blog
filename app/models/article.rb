class Article < ApplicationRecord
  include Slugable

  belongs_to :user

  enum status: [:draft, :published]

  validates :title, presence: true
  validates :text, presence: true
  validates :user, presence: true

  after_save :log_creation
  before_create :slugify
  before_save :check_if_publishing

  def to_param
    slug
  end

  def publishing?
    @publishing
  end

  private

  def log_creation
    logger.info("New article: #{title} created")
  end

  def slugify
    self.slug = to_slug(title)
  end

  def check_if_publishing
    @publishing = status_was == 'draft' && status == 'published'
  end
end
