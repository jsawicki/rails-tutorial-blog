class NotificationChannel < ApplicationCable::Channel
  def subscribed
    stream_for 'notification'
  end
end
