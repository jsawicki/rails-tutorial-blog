$(document).on "turbolinks:load", ->
  App.notification = App.cable.subscriptions.create {
      channel: "NotificationChannel"
    },
    connected: ->
      console.log("ws connected")

    received: (data) ->
      console.log("data received %o", data)
      @notify(data.msg, data.status)

    notify: (msg, status) ->
      document.getElementById("alerts").
        innerHTML = """
          <div class="alert alert-#{status} alert-dismissible fade show" role="alert">
            #{msg}
            <button class="close" type="button" data-dismiss="alert" />
            <span aria-hidden="true">&times</span>
          </div>
        """
