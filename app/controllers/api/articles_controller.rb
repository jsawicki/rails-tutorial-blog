module Api
  class ArticlesController < Api::ApplicationController
    def index
      authorize Article
      @articles = policy_scope(Article)
    end
  end
end

