class ArticleMailer < ApplicationMailer

  def article_published_email(article)
    author = article.user
    @title = article.title

    mail(to: author.email, subject: 'Article published')
  end
end
