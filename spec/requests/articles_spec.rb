require 'rails_helper'

RSpec.describe ArticlesController do
  context 'when user not logged in' do
    it 'cannot see articles' do
      get articles_path

      expect(response).to redirect_to new_user_session_path
    end

    # similar to cannot create, edit, delete, see single article
  end

  context 'when logged in' do
    let(:user) { create(:user) }
    before { login_as(user) }

    it 'can see all created articles' do
      create(:article, user: user, title: 'Dogs vs Cats')
      create(:article, title: 'Other user article not published yet')

      get articles_path

      expect(response.status).to eq 200
      expect(response.body).to include 'Dogs vs Cats'
      expect(response.body).to_not include 'Other user article'
    end

    it 'can create new article' do
      post articles_path,
           params: { article: { title: 'title', text: 'text' } }

      article = Article.last

      expect(article.title).to eq 'title'
      expect(article.text).to eq 'text'
      expect(article.user).to eq user
      expect(article).to be_draft
    end

    it 'can edit owned article' do
      article = create(:article, user: user, title: 'Dogs vs Cats')

      put article_path(article),
          params: { article: { title: 'New title' } }
      article.reload

      expect(article.title).to eq 'New title'
    end

    it 'sends email after article is published' do
      article = create(:article, status: :draft, user: user)

      expect do
        put article_path(article), params: { article: { status: 'published' } }
      end.to change { ActionMailer::Base.deliveries.count }.by(1)
    end

    it 'email is not changed while status not changed' do
      article = create(:article, status: :draft, user: user)

      expect do
        put article_path(article), params: { article: { title: 'New title'} }
      end.to_not change { ActionMailer::Base.deliveries.count }
    end

    it 'cannot edit not owned article' do
      article = create(:article, title: 'Other user article')

      put article_path(article),
          params: { article: { title: 'My precious' } }
      article.reload

      expect(response.body).to include 'redirected'
      expect(article.title).to eq 'Other user article'
    end
  end
end
