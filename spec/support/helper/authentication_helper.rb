module AuthenticationHelper
  def google_sign_in_as(user)
    stub_oauth(
      :google_oauth2,
      email: user.email
    )
    visit user_google_oauth2_omniauth_authorize_path
  end

  def facebook_sign_in_as(user)
    stub_oauth(
      :facebook,
      email: user.email
    )
    visit user_facebook_omniauth_authorize_path
  end

  def sign_in_as(user)
    visit new_user_session_path

    fill_in 'Email', with: user.email
    fill_in 'Password', with: user.password
    click_button 'Log in'
  end

  def stub_oauth(provider, options = {})
    OmniAuth.config.add_mock(
      provider,
      info: {
        email: options[:email],
      },
      provider: provider,
      uid: '123'
    )
  end
end
