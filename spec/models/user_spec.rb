RSpec.describe User do
  it { should have_many(:articles).dependent(:destroy) }

  it 'has token generation and finder' do
    user = create(:user)

    token = user.token
    found_user = User.from_token(token)

    expect(found_user).to eq user
  end
end
