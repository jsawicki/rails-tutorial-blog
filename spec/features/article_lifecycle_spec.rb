 require 'rails_helper'

 RSpec.feature 'Article' do
  include AuthenticationHelper

  it 'can be created by any logged in user' do
    author = create(:user)

    sign_in_as(author)
    visit new_article_path
    fill_in 'Article title', with: 'My title'
    fill_in 'Article text', with: 'My body'
    click_button 'Create Article'

    expect(page).to have_text 'My title'
    expect(page).to have_text 'My body'
    expect(page).to have_text author.email
    expect(page).to have_text 'draft'
  end

  it 'draft can be edited by article author' do
    author = create(:user)
    article = create(:article, user: author)

    sign_in_as(author)
    visit edit_article_path(article)
    fill_in 'Article title', with: 'My updated title'
    fill_in 'Article text', with: 'My updated body'
    click_button 'Update Article'

    expect(page).to have_text 'My updated title'
    expect(page).to have_text 'My updated body'
  end
 end

