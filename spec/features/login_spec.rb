 require 'rails_helper'

 RSpec.feature 'Login' do
  include AuthenticationHelper

  it 'using FB creates new user if not exist' do
    user = build(:user)

    expect do
      facebook_sign_in_as(user)
    end.to change { User.count }.by(1)

    expect(User.last.email).to eq user.email
  end

  it 'using Google creates new user if not exist' do
    user = build(:user)

    expect do
      google_sign_in_as(user)
    end.to change { User.count }.by(1)

    expect(User.last.email).to eq user.email
  end

  context 'normal email/password login' do
    it 'does not create new user' do
      user = build(:user)

      sign_in_as(user)

      expect(page).to have_text 'Invalid Email or password.'
    end

    it 'works for already registered user' do
      user = create(:user)

      sign_in_as(user)

      expect(page).to have_text 'Signed in successfully.'
    end
  end
 end
