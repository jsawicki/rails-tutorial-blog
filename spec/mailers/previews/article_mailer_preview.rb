class ArticleMailerPreview < ActionMailer::Preview
  def article_published_email
    article = Article.new(title: 'This is article title',
                            text: 'Article text',
                            user: User.new(email: 'mailer@preview.pl'))

    ArticleMailer.article_published_email(article)
  end
end
