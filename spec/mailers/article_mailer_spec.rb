require 'rails_helper'

RSpec.describe ArticleMailer do
  let(:article) { build(:article) }
  let(:mail) { ArticleMailer.article_published_email(article) }

  it 'renders the headers' do
    expect(mail.subject).to eq 'Article published'
    expect(mail.to).to eq [article.user.email]
    expect(mail.from).to eq ['from@example.com']
  end

  it 'renders the body' do
    expect(mail.body.encoded).to match('Huray!')
    expect(mail.body.encoded).to match(article.title)
  end
end
